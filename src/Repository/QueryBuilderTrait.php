<?php


namespace App\Repository;

use App\Contract\CriteriaInterface;
use App\Contract\PolicyStrategyFilterInterface;
use App\Domain\Model\Category;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

trait QueryBuilderTrait
{
    protected function addFilter(QueryBuilder $queryBuilder, $filterCode, $filterValue) : bool
    {
        $applied = false;
        switch ($filterCode) {
            case PolicyStrategyFilterInterface::class: {
                $this->addPolicyFilter($queryBuilder, $filterValue);
                $applied = true;
                break;
            }
            case "id": {
                $this->addIdentityFilter($queryBuilder, $filterValue);
                $applied = true;
                break;
            }
        }
        return $applied;
    }

    protected function addPolicyFilter(QueryBuilder $queryBuilder, $value, $tableAlias = 't1')
    {
        switch ($value) {
            case PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE: {
                $queryBuilder->andWhere($queryBuilder->expr()->isNull($tableAlias .'.deleted_at'));
                break;
            }
            case PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_INACTIVE: {
                $queryBuilder->andWhere($queryBuilder->expr()->isNotNull($tableAlias .'.deleted_at'));
                break;
            }
            default: {}
        }
    }

    protected function addIdentityFilter(QueryBuilder $queryBuilder, $ids, $tableAlias = 't1')
    {
        if (!is_array($ids)) {
            $ids = (array)$ids;
        }
        $queryBuilder->andWhere($queryBuilder->expr()->in($tableAlias . '.id', $ids));
    }

    protected function addOrdering(QueryBuilder $queryBuilder, CriteriaInterface $criteria, $allowOrderingFields = [])
    {
        $customOrdering = false;
        foreach ($criteria->getOrdering() as $field => $orderDirection) {
            if (!in_array(strtolower($orderDirection), ['asc', 'desc'])) {
                continue;
            }

            if (!in_array($field, $allowOrderingFields)) {
                continue;
            }

            $queryBuilder->addOrderBy('t1.' . $field, $orderDirection);
            $customOrdering = true;
        }
        if (!$customOrdering) {
            $queryBuilder->addOrderBy('t1.' . self::ORDER_FIELD_UPDATE_AT, 'asc');
        }
    }

    protected function createBaseQueryBuilderByCriteria($entityClass, CriteriaInterface $criteria, EntityManager $em)
    {
        $offset = ($criteria->getPage() - 1 <= 0) ? 0 : (($criteria->getPage() - 1) * $criteria->getSize());

        $qb = $em->createQueryBuilder();
        $qb->select('t1')
            ->from($entityClass, 't1')
            ->setMaxResults($criteria->getSize())
            ->setFirstResult($offset);
        $this->prepareFilters($qb,$criteria);
        return $qb;
    }
}
