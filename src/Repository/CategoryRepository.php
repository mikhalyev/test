<?php

namespace App\Repository;

use App\Common\Criteria;
use App\Contract\CategoryRepositoryInterface;
use App\Contract\CriteriaInterface;
use App\Contract\PolicyStrategyFilterInterface;
use App\Domain\Model\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository implements CategoryRepositoryInterface
{
    use QueryBuilderTrait;

    const ORDER_FIELD_CREATED_AT = 'created_at';

    const ORDER_FIELD_UPDATE_AT = 'updated_at';

    protected $allowedOrdering = [self::ORDER_FIELD_CREATED_AT, self::ORDER_FIELD_UPDATE_AT];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    protected function prepareFilters(QueryBuilder $queryBuilder, CriteriaInterface $criteria)
    {
        foreach ($criteria->getFilters() as $filterCode => $filterValue)
        {
            if ($this->addFilter($queryBuilder, $filterCode, $filterValue)) {
                continue;
            }
        }
    }

    /**
     * Create configured QueryBuilder by criteria
     *
     * @param CriteriaInterface $criteria
     * @return QueryBuilder
     */
    protected function getQueryBuilderByCriteria(CriteriaInterface $criteria) : QueryBuilder
    {
        $qb = $this->createBaseQueryBuilderByCriteria(Category::class, $criteria, $this->_em);

        $this->addOrdering($qb, $criteria, $this->allowedOrdering);

        $this->prepareFilters($qb, $criteria);

        return $qb;
    }

    /**
     * Build query by criteria
     * @param CriteriaInterface $criteria
     * @return Query
     */
    public function buildQueryByCriteria(CriteriaInterface $criteria) : Query
    {
        $qb = $this->getQueryBuilderByCriteria($criteria);
        return $qb->getQuery();
    }

    /**
     * @param Query $query
     * @return Paginator
     */
    public function getPaginator(Query $query) : Paginator
    {
        $paginator = new Paginator($query, $fetchJoinCollection = true);
        return $paginator;
    }

    public function findById(int $id, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : ?Category
    {
        $items = $this->findByIds([$id], $policyStrategyFilter);
        return empty($items) ? null : current($items);
    }

    public function findByIds(array $ids, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array
    {
        if (empty($ids)) {
            return [];
        }
        $criteria = new Criteria();
        $criteria->addFilter(PolicyStrategyFilterInterface::class, $policyStrategyFilter);
        $criteria->addFilter('id', $ids);
        $criteria->setSize(count($ids));

        $query = $this->buildQueryByCriteria($criteria);
        $items = $query->getResult();
        return $items;
    }

    public function save(Category $item) {
        $this->_em->persist($item);
        $this->_em->flush();
    }
}
