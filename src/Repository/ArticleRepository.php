<?php

namespace App\Repository;
use App\Common\Criteria;
use App\Contract\ArticleRepositoryInterface;
use App\Contract\ArticleServiceInterface;
use App\Contract\CriteriaInterface;
use App\Contract\PolicyStrategyFilterInterface;
use App\Domain\Model\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ArticleRepository extends ServiceEntityRepository implements ArticleRepositoryInterface
{
    use QueryBuilderTrait;

    const ORDER_FIELD_CREATED_AT = 'created_at';

    const ORDER_FIELD_UPDATE_AT = 'updated_at';

    const FILTER_CODE_CATEGORY = 'category';

    protected $allowedOrdering = [self::ORDER_FIELD_CREATED_AT, self::ORDER_FIELD_UPDATE_AT];

    protected $allowFilterCodes = [
        self::FILTER_CODE_CATEGORY => 't2.id' // TODO: использовать название фильтра из Общего интерфйса Сервиса
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    protected function prepareFilters(QueryBuilder $queryBuilder, CriteriaInterface $criteria)
    {
        $isJoinedCategories = false;
        foreach ($criteria->getFilters() as $filterCode => $filterValue) {

            if ($this->addFilter($queryBuilder, $filterCode, $filterValue)) {
                continue;
            }

            if (!array_key_exists($filterCode, $this->allowFilterCodes)) {
                continue;
            }
            if (!$isJoinedCategories) {
                $queryBuilder->innerJoin('t1.categories', 't2');
                $isJoinedCategories = true;
            }

            $paramKey = uniqid(':field_');
            $sqlParam = $this->allowFilterCodes[$filterCode];
            $queryBuilder->andWhere($sqlParam . ' = ' . $paramKey)
                ->setParameter($paramKey, $filterValue);
        }
    }

    /**
     * Create configured QueryBuilder by criteria
     *
     * @param CriteriaInterface $criteria
     * @return QueryBuilder
     */
    protected function getQueryBuilderByCriteria(CriteriaInterface $criteria) : QueryBuilder
    {
        $qb = $this->createBaseQueryBuilderByCriteria(Article::class, $criteria, $this->_em);
        $this->addOrdering($qb, $criteria, $this->allowedOrdering);
        $this->prepareFilters($qb, $criteria);
        return $qb;
    }

    /**
     * @param Criteria $criteria
     * @return Query
     * @deprecated This method need only for custom realisation context search engine in SearchService
     */
    public function getSearchQueryByCriteria(Criteria $criteria) : Query
    {
        $qb = $this->getQueryBuilderByCriteria($criteria);
        $contextSearchString = $criteria->getFilterValue(ArticleServiceInterface::FILTER_CODE_CONTEXT_SEARCH);
        if ($contextSearchString) {
            $qb
                ->andWhere('MATCH_AGAINST(t1.title, t1.text, :string) > 0')
                ->setParameter(':string', $contextSearchString);
        }
        return $qb->getQuery();
    }

    /**
     * Build query by criteria
     * @param CriteriaInterface $criteria
     * @return Query
     */
    public function buildQueryByCriteria(CriteriaInterface $criteria) : Query
    {
        $qb = $this->getQueryBuilderByCriteria($criteria);
        return $qb->getQuery();
    }

    /**
     * @param Query $query
     * @return Paginator
     */
    public function getPaginator(Query $query) : Paginator
    {
        $paginator = new Paginator($query, $fetchJoinCollection = true);
        return $paginator;
    }

    public function findById(int $id, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : ?Article
    {
        $items = $this->findByIds([$id], $policyStrategyFilter);
        return empty($items) ? null : current($items);
    }


    public function findByIds(array $ids, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array
    {
        if (empty($ids)) {
            return [];
        }
        $criteria = new Criteria();
        $criteria->addFilter(PolicyStrategyFilterInterface::class, $policyStrategyFilter);
        $criteria->addFilter('id', $ids);
        $criteria->setSize(count($ids));

        $query = $this->buildQueryByCriteria($criteria);
        $items = $query->getResult();
        return $items;
    }

    public function save(Article $item) {
        $this->_em->persist($item);
        $this->_em->flush();
    }
}
