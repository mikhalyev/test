<?php

namespace App\Domain\Service;

use App\Contract\ArticleSearchServiceInterface;
use App\Contract\ArticleServiceInterface;
use App\Contract\CriteriaInterface;
use App\Contract\ModelSearchResultInterface;
use App\Contract\PaginatorInterface;
use App\Domain\Model\Article;
use App\Repository\ArticleRepository;

class ArticleSearchService implements ArticleSearchServiceInterface, ModelSearchResultInterface
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;


    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    protected function clearSearchString($rawQuery)
    {
        $q = $rawQuery;
        $q = iconv('utf-8', 'utf-8//ignore', $q);
        $q = preg_replace('/[^\pL\pN\pP\pS\pZ]/u', '', $q);
        $q = preg_replace('/\s+/', ' ', $q);
        $q = explode(' ', $q);
        $tmp = [];
        foreach ($q as $term) {
            $term = strip_tags($term);
            $term = preg_replace('/[^ a-zа-яё\d]/ui', '', $term);
            $term = strtolower($term);
            if (strlen($term) > 1) {
                $tmp[] = $term;
            }
        }
        $q = $tmp;
        unset($tmp);
        $q = implode(' ', $q);
        return $q;
    }

    public function search(CriteriaInterface $criteria, ?PaginatorInterface &$paginator = null): array
    {
        $articles = [];
        if ($paginator) {
            $paginator->setPage($criteria->getPage());
            $paginator->setSize($criteria->getSize());
        }

        $contextSearchString = $criteria->getFilterValue(ArticleServiceInterface::FILTER_CODE_CONTEXT_SEARCH);
        $contextSearchString = $this->clearSearchString($contextSearchString);
        $criteria->addFilter(ArticleServiceInterface::FILTER_CODE_CONTEXT_SEARCH, $contextSearchString);

        $query = $this->articleRepository->getSearchQueryByCriteria($criteria);
        $queryPager = $this->articleRepository->getPaginator($query);
        $articles = iterator_to_array($queryPager->getIterator());

        if ($paginator) {
            $paginator->setItems($articles);
            $paginator->setTotal($queryPager->count());
        }
        return $articles;
    }
}
