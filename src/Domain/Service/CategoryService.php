<?php

namespace App\Domain\Service;

use App\Contract\CategoryServiceInterface;
use App\Contract\CriteriaInterface;
use App\Contract\PaginatorInterface;
use App\Contract\PolicyStrategyFilterInterface;
use App\Domain\Model\Category;
use App\Dto\Category as CategoryDto;
use App\Exception\InvalidDataException;
use App\Exception\ModelNotFoundException;
use App\Repository\CategoryRepository;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryService implements CategoryServiceInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var CategoryBuilderService
     */
    protected $builderService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        CategoryRepository $categoryRepository,
        CategoryBuilderService $builderService,
        ValidatorInterface $validator
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->builderService = $builderService;
        $this->validator = $validator;
    }

    /**
     * @param CriteriaInterface $criteria
     * @param PaginatorInterface|null $paginator
     * @param int $policyStrategyFilter
     * @return array
     */
    public function search(CriteriaInterface $criteria, ?PaginatorInterface &$paginator = null, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array
    {
        $items = [];

        if ($paginator) {
            $paginator->setPage($criteria->getPage());
            $paginator->setSize($criteria->getSize());
        }

        $criteria->addFilter(PolicyStrategyFilterInterface::class, $policyStrategyFilter);
        $queryPager = $this->categoryRepository->getPaginator($this->categoryRepository->buildQueryByCriteria($criteria));
        $items = iterator_to_array($queryPager->getIterator());

        if ($paginator) {
            $paginator->setItems($items);
            $paginator->setTotal($queryPager->count());
        }

        return $items;
    }

    /**
     * @param int $id
     * @param int $policyStrategyFilter
     * @return Category|null
     */
    public function getById(int $id, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : ?Category
    {
        $item = $this->categoryRepository->findById($id, $policyStrategyFilter);
        return $item;
    }

    /**
     * @param array $ids
     * @param int $policyStrategyFilter
     * @return Category[]
     */
    public function getByIds(array $ids, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array
    {
        return $this->categoryRepository->findByIds($ids, $policyStrategyFilter);
    }

    /**
     * @param CategoryDto $dto
     * @return Category
     * @throws ValidatorException
     */
    public function create(CategoryDto $dto) : Category
    {
        $this->validateDto($dto);
        $item = $this->builderService->fillEntity($dto);
        $this->categoryRepository->save($item);
        return $item;
    }

    /**
     * @param int $id
     * @param CategoryDto $dto
     * @return Category
     * @throws \RuntimeException
     */
    public function update(int $id, CategoryDto $dto) : Category
    {
        $this->validateDto($dto);
        $item = $this->categoryRepository->findById($id);
        if (!$item) {
            throw new \RuntimeException('Category with id=' . $id . ' not found!');
        }
        $item = $this->builderService->fillEntity($dto, $item);
        $this->categoryRepository->save($item);
        return $item;
    }

    /**
     * @param CategoryDto $dto
     */
    protected function validateDto(CategoryDto $dto)
    {
        $errors = $this->validator->validate($dto);
        $errorsStringList = [];
        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorsStringList[] = $error->getMessage();
            }
            throw new InvalidDataException(implode('. ', $errorsStringList));
        }
    }

    /**
     * @param int $id
     * @throws \RuntimeException
     */
    public function remove(int $id) : void
    {
        $item = $this->categoryRepository->findById($id);
        if (!$item) {
            throw new ModelNotFoundException('Category with id=' . $id . ' not found!');
        }

        $item->setDeletedAt(new \DateTime());
        $this->categoryRepository->save($item);
    }
}
