<?php

namespace App\Domain\Service;

use App\Domain\Model\Category;
use App\Dto\Category as CategoryDto;

class CategoryBuilderService
{
    public function fillEntity(CategoryDto $dto, ?Category $model = null): ?Category
    {
        if (!$model) {
            $model = new Category();
        }

        if ($dto->getName() !== null) {
            $model->setName($dto->getName());
        }

        return $model;
    }
}
