<?php

namespace App\Domain\Service;

use App\Contract\CategoryServiceInterface;
use App\Domain\Model\Article;
use App\Dto\Article as ArticleDto;
use App\Exception\InvalidDataException;
use App\Exception\ModelNotFoundException;

class ArticleBuilderService
{
    /**
     * @var CategoryServiceInterface
     */
    protected $categoryService;

    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function fillEntity(ArticleDto $articleDto, ?Article $article = null): ?Article
    {
        if (!$article) {
            $article = new Article();
        }

        if ($articleDto->getTitle() !== null) {
            $article->setTitle($articleDto->getTitle());
        }

        if ($articleDto->getText() !== null) {
            $article->setText($articleDto->getText());
        }

        $this->prepareCategories($article, $articleDto);

        return $article;
    }

    protected function prepareCategories(Article $article, ArticleDto $articleDto)
    {
        if (!$article->getId() && ($articleDto->getCategory() == null || empty($articleDto->getCategory()))) {
            throw new InvalidDataException("Empty categories for Article");
        }

        $categoryIds = (array)$articleDto->getCategory();
        $categories = $this->categoryService->getByIds($categoryIds);

        if (!empty($categoryIds)) {
            $article->clearCategory();
        }

        $foundCategoryIds = [];
        foreach ($categories as $category) {
            $article->addCategory($category);
            $foundCategoryIds[] = $category->getId();
        }

        $notValidIds = array_diff($categoryIds, $foundCategoryIds);
        if (!empty($notValidIds)) {
            throw new ModelNotFoundException('Categories with ids:[' . implode(',', $notValidIds) . '] not found!');
        }
    }
}
