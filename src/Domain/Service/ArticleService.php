<?php

namespace App\Domain\Service;

use App\Contract\ArticleSearchServiceInterface;
use App\Contract\ArticleServiceInterface;
use App\Contract\CriteriaInterface;
use App\Contract\IdentitySearchResultInterface;
use App\Contract\ModelSearchResultInterface;
use App\Contract\PaginatorInterface;
use App\Contract\PolicyStrategyFilterInterface;
use App\Domain\Model\Article;
use App\Exception\InvalidDataException;
use App\Exception\ModelNotFoundException;
use App\Repository\ArticleRepository;
use App\Dto\Article as ArticleDto;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ArticleService implements ArticleServiceInterface
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var ArticleBuilderService
     */
    private $builderService;

    /**
     * @var ArticleSearchServiceInterface
     */
    private $articleSearchService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ArticleService constructor.
     * @param ArticleRepository $articleRepository
     * @param ArticleBuilderService $builderService
     * @param ArticleSearchServiceInterface $articleSearchService
     * @param ValidatorInterface $validator
     */
    public function __construct(
        ArticleRepository $articleRepository,
        ArticleBuilderService $builderService,
        ArticleSearchServiceInterface $articleSearchService,
        ValidatorInterface $validator
    ) {
        $this->articleRepository = $articleRepository;
        $this->builderService = $builderService;
        $this->articleSearchService = $articleSearchService;
        $this->validator = $validator;
    }

    /**
     * @param CriteriaInterface $criteria
     * @param PaginatorInterface|null $paginator
     * @param int $policyStrategyFilter
     * @return array
     */
    public function search(CriteriaInterface $criteria, ?PaginatorInterface &$paginator = null, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array
    {
        $articles = [];

        if ($paginator) {
            $paginator->setPage($criteria->getPage());
            $paginator->setSize($criteria->getSize());
        }

        if ($criteria->getFilter(self::FILTER_CODE_CONTEXT_SEARCH)) {
            $collection = $this->articleSearchService->search($criteria, $paginator);
            if ($this->articleSearchService instanceof IdentitySearchResultInterface) {
                $articles = $this->articleRepository->findByIds($collection, $policyStrategyFilter);
                $result = array_flip($collection);
                foreach ($articles as $article) {
                    $result[$article->getId()] = $article;
                }
                $articles = array_values(array_filter($result, function ($item) {
                    return $item instanceof Article;
                }));
            } else if ($this->articleSearchService instanceof ModelSearchResultInterface) {
                $articles = $collection;
            } else {
                throw new \RuntimeException("Search Service should be implement one of the next interface (IdentitySearchResultInterface | ModelSearchResultInterface)");
            }
        } else {
            $criteria->addFilter(PolicyStrategyFilterInterface::class, $policyStrategyFilter);
            $queryPager = $this->articleRepository->getPaginator($this->articleRepository->buildQueryByCriteria($criteria));
            $articles = iterator_to_array($queryPager->getIterator());

            if ($paginator) {
                $paginator->setItems($articles);
                $paginator->setTotal($queryPager->count());
            }
        }
        return $articles;
    }

    public function getById(int $id, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : ?Article
    {
        $item = $this->articleRepository->findById($id, $policyStrategyFilter);
        return $item;
    }

    /**
     * @param ArticleDto $dto
     * @return Article
     * @throws InvalidDataException
     */
    public function create(ArticleDto $dto) : Article
    {
        $this->handleValidationErrors($this->validator->validate($dto));

        $article = $this->builderService->fillEntity($dto);
        $this->articleRepository->save($article);
        return $article;
    }

    /**
     * @param int $id
     * @param ArticleDto $dto
     * @return Article
     * @throws ModelNotFoundException|InvalidDataException
     */
    public function update(int $id, ArticleDto $dto) : Article
    {
        $this->handleValidationErrors($this->validator->validate($dto, null, ['update']));

        $article = $this->articleRepository->findById($id);
        if (!$article) {
            throw new ModelNotFoundException('Article with id=' . $id . ' not found!', 404);
        }

        $article = $this->builderService->fillEntity($dto, $article);
        $this->articleRepository->save($article);
        return $article;
    }

    /**
     * @param $errors
     * @throws InvalidDataException
     */
    public function handleValidationErrors($errors)
    {
        $errorsStringList = [];
        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorsStringList[] = $error->getMessage();
            }
            throw new InvalidDataException(implode('. ', $errorsStringList));
        }
    }

    /**
     * @param int $id
     * @throws ModelNotFoundException
     */
    public function remove(int $id) : void
    {
        $item = $this->articleRepository->findById($id);
        if (!$item) {
            throw new ModelNotFoundException('Article with id=' . $id . ' not found!', 404);
        }

        $item->setDeletedAt(new \DateTime());
        $this->articleRepository->save($item);
    }
}
