<?php

namespace App\Dto;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Category
 * @package App\Dto
 */
final class Category
{
    /**
     * @var string
     * @Type("string")
     * @Assert\NotBlank(message="Category name is erquired")
     * @Assert\Length(max=255, maxMessage="Name of category can't be more than {{ limit }} chars.")
     */
    private $name;

    /**
     * Category constructor.
     * @param string $name
     */
    public function __construct(string $name = '')
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
