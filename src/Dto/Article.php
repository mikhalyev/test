<?php

namespace App\Dto;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Article
 * @package App\Dto
 */
final class Article
{
    /**
     * @var string
     * @Type("string")
     * @Assert\NotBlank(message="Article title is erquired")
     * @Assert\NotBlank(allowNull=true, message="Article title is empty", groups={"update"})
     * @Assert\Length(max=255, maxMessage="Title can not be more than {{ limit }} chars.", groups={"update", "Default"})
     */
    private $title;

    /**
     * @var string
     * @Type("string")
     * @Assert\NotBlank(message="Article text is erquired")
     * @Assert\NotBlank(allowNull=true, message="Article text is empty", groups={"update"})
     */
    private $text;

    /**
     * @var array
     * @Type("array<integer>")
     * @Assert\NotBlank(message="Categories can not by empty for Article, categories is array of ID category for link")
     */
    private $category;

    /**
     * Article constructor.
     * @param string $title
     * @param string $text
     * @param array $category
     */
    public function __construct(string $title = '', string $text = '', array $category = [])
    {
        $this->title = $title;
        $this->text = $text;
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return array
     */
    public function getCategory(): ?array
    {
        return $this->category;
    }
}
