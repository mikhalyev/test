<?php

namespace App\Dto;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Token
 * @package App\Dto
 */
final class Token
{
    /**
     * @var string
     * @Type("string")
     * @Assert\NotBlank(message="Login is erquired")
     * @Assert\Length(max=32, maxMessage="Login can not be more than {{ limit }} chars.")
     */
    private $login;

    /**
     * @var string
     * @Type("string")
     * @Assert\NotBlank(message="Password is erquired")
     * @Assert\Length(max=32, maxMessage="Password can not be more than {{ limit }} chars.")
     */
    private $pass;

    /**
     * Token constructor.
     * @param string $login
     * @param string $pass
     */
    public function __construct(string $login = '', string $pass = '')
    {
        $this->login = $login;
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }
}
