<?php


namespace App\Common;


use App\Contract\CriteriaInterface;

class Criteria implements CriteriaInterface
{
    /**
     * @var int
     */
    protected $page = 1;

    /**
     * @var int
     */
    protected $size = 10;

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @var array
     */
    protected $order = [];

    /**
     * Get page
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * Set page for criteria
     * @param int $page
     * @return CriteriaInterface
     */
    public function setPage(int $page): CriteriaInterface
    {
        $this->page = $page;
        return $this;
    }

    /**
     * Get size of page
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * Set amount elements for per page
     * @param int $size
     * @return CriteriaInterface
     */
    public function setSize(int $size): CriteriaInterface
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Get filters list
     * @return array
     */
    public function getFilters(array $codes = []) : array
    {
        return array_filter($this->filters, function ($filterCode) use ($codes) {

            if (empty($codes)) {
                return true;
            }

            return in_array($filterCode, $codes);

        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Get filter by code
     * @param string $code
     * @return array|null
     */
    public function getFilter(string $code)
    {
        if (array_key_exists($code, $this->filters)) {
            return [$code => $this->filters[$code]];
        }
        return null;
    }

    /**
     * Get filter by code
     * @param string $code
     * @return array|null
     */
    public function getFilterValue(string $code)
    {
        if (array_key_exists($code, $this->filters)) {
            return $this->filters[$code];
        }
        return null;
    }

    /**
     * Add filter
     * @param String $filterCode
     * @param mixed $filterValue
     * @return CriteriaInterface
     */
    public function addFilter(String $filterCode, $filterValue, $skipForEmpty = true): CriteriaInterface
    {
        if (!$skipForEmpty || ($skipForEmpty && !empty($filterValue))) {
            $this->filters[$filterCode] = $filterValue;
        }
        return $this;
    }

    /**
     * Add filter
     * @param array $filters
     * @return CriteriaInterface
     */
    public function setFilters(array $filters, $skipForEmpty = true): CriteriaInterface
    {
        $this->clearFilters();
        foreach ($filters as $filterCode => $filterValue) {
            $this->addFilter($filterCode, $filterValue, $skipForEmpty);
        }
        return $this;
    }

    /**
     * Remove all filters
     * @return CriteriaInterface
     */
    public function clearFilters(): CriteriaInterface
    {
        $this->filters = [];
        return $this;
    }

    /**
     * Remove filter by it key
     * @param string $filterCode
     * @return CriteriaInterface
     */
    public function removeFilter(string $filterCode): CriteriaInterface
    {
        if (array_key_exists($filterCode, $this->filters)) {
            unset($this->filters[$filterCode]);
        }

        return $this;
    }

    /**
     * Get ordering
     * @return array
     */
    public function getOrdering(): array
    {
        return $this->order;
    }

    public function addOrder(string $key, $ordering = self::ORDER_ASC)
    {
        if (!in_array(strtolower($ordering), [self::ORDER_ASC, self::ORDER_DESC])) {
            $ordering = self::ORDER_ASC;
        }
        $this->order[$key] = $ordering;
        return $this;
    }

    public function clearOrdering()
    {
        $this->order = [];
        return $this;
    }


}
