<?php

namespace App\Contract;

use App\Domain\Model\Category;
use App\Dto\Category as CategoryDto;

interface CategoryServiceInterface
{
    public function search(CriteriaInterface $criteria, ?PaginatorInterface &$paginator = null, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array;

    /**
     * @param int $id
     * @param int $policyStrategyFilter
     * @return Category|null
     * @throws \Throwable
     */
    public function getById(int $id, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : ?Category;

    /**
     * @param array $ids
     * @param int $policyStrategyFilter
     * @return Category[]
     */
    public function getByIds(array $ids, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array;

    /**
     * @param CategoryDto $dto
     * @return Category
     */
    public function create(CategoryDto $dto) : Category;

    /**
     * @param int $id
     * @param CategoryDto $dto
     * @return Category
     */
    public function update(int $id, CategoryDto $dto) : Category;

    /**
     * @param int $id
     */
    public function remove(int $id) : void;
}
