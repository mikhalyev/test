<?php

namespace App\Contract;

interface CriteriaInterface
{
    const ORDER_ASC = 'asc';

    const ORDER_DESC = 'desc';

    public function getPage() : int;

    public function setPage(int $page) : CriteriaInterface;

    public function getSize() : int;

    public function setSize(int $size) : CriteriaInterface;

    public function getFilters(array $codes = []) : array;

    public function getFilter(string $code);

    public function getFilterValue(string $code);

    public function addFilter(String $filterCode, $filterValue, $skipForEmpty = true) : CriteriaInterface;

    public function setFilters(array $filters, $skipForEmpty = true) : CriteriaInterface;

    public function clearFilters() : CriteriaInterface;

    public function removeFilter(string $filterCode) : CriteriaInterface;

    public function getOrdering() : array ;

    public function addOrder(string $key, $ordering = self::ORDER_ASC);

    public function clearOrdering();


}
