<?php

namespace App\Contract;

use App\Domain\Model\Article;
use App\Dto\Article as ArticleDto;

interface ArticleServiceInterface
{
    const FILTER_CODE_CONTEXT_SEARCH = 'context_search_string';

    const FILTER_CODE_CATEGORY = 'category';

    public function search(CriteriaInterface $criteria, ?PaginatorInterface &$paginator = null, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE) : array;

    public function getById(int $id, $policyStrategyFilter = PolicyStrategyFilterInterface::POLICY_STRATEGY_FILTER_ACTIVE): ?Article;

    public function create(ArticleDto $dto) : Article;

    public function update(int $id, ArticleDto $dto) : Article;

    public function remove(int $id): void;
}
