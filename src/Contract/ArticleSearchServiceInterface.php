<?php

namespace App\Contract;

use App\Common\Criteria;
use App\Domain\Model\Article;

/**
 * Interface described Search Service class
 * Also class implemented this interface should be implement on of the empty
 * IdentitySearchResultInterface.php interface or ModelSearchResultInterface.php interface
 * Interface ArticleSearchServiceInterface
 * @package App\Contract
 */
interface ArticleSearchServiceInterface
{
    /**
     * @param CriteriaInterface $criteria
     * @param PaginatorInterface|null $paginator
     * @return array Array of article identity
     */
    public function search(CriteriaInterface $criteria, ?PaginatorInterface &$paginator = null) : array;
}
