<?php

namespace App\Contract;

/**
 * Interface describe a principle returned items by active status
 * Variants All|ACTIVE|INACTIVE
 *
 * @package App\Contract
 */
interface PolicyStrategyFilterInterface
{
    const POLICY_STRATEGY_FILTER_ALL = 0;

    const POLICY_STRATEGY_FILTER_ACTIVE = 1;

    const POLICY_STRATEGY_FILTER_INACTIVE = -1;
}
