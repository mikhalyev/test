<?php
namespace App\Contract;

interface PaginatorInterface
{
    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @param int $count
     */
    public function setTotal(int $count): void;

    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @param array $items
     */
    public function setItems(array $items): void;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @param int $page
     */
    public function setPage(int $page): void;

    /**
     * @return int
     */
    public function getPages(): int;

    /**
     * @return int
     */
    public function getSize(): int;

    /**
     * @param int $size
     */
    public function setSize(int $size): void;
}
