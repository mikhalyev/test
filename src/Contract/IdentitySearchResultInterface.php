<?php

namespace App\Contract;

/**
 * Interface describe a principle returned items from search method described in Interface ArticleSearchServiceInterface.php
 * Result should be array where values is a ID of Model objects
 * Interface ModelSearchResultInterface
 * @package App\Contract
 */
interface IdentitySearchResultInterface
{
}
