<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191202184931 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('alter table article modify created_at timestamp not null;');
        $this->addSql('alter table article modify updated_at timestamp not null;');
        $this->addSql('alter table article modify deleted_at timestamp null;');

        $this->addSql('alter table category modify created_at timestamp not null;');
        $this->addSql('alter table category modify updated_at timestamp not null;');
        $this->addSql('alter table category modify deleted_at timestamp null;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('alter table article modify created_at datetime not null;');
        $this->addSql('alter table article modify updated_at datetime not null;');
        $this->addSql('alter table article modify deleted_at datetime null;');

        $this->addSql('alter table category modify created_at datetime not null;');
        $this->addSql('alter table category modify updated_at datetime not null;');
        $this->addSql('alter table category modify deleted_at datetime null;');
    }
}
