<?php

namespace App\Security;

use App\Dto\Token;
use App\Exception\InvalidDataException;
use App\Exception\ModelNotFoundException;
use App\Repository\UserRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TokenService
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * TokenService constructor.
     * @param UserRepository $userRepository
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        UserRepository $userRepository,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->userRepository = $userRepository;
        $this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function create(Token $dto)
    {
        $errors = $this->validator->validate($dto);
        $errorsStringList = [];
        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorsStringList[] = $error->getMessage();
            }
            throw new InvalidDataException(implode('. ', $errorsStringList));
        }
        $user = $this->userRepository->getByLogin($dto->getLogin());
        if (!$user) {
            throw new ModelNotFoundException("User not found by login password");
        }

        if (!$this->passwordEncoder->isPasswordValid($user, $dto->getPass())) {
            throw new ModelNotFoundException("User not found by login password");
        }

        $token = Uuid::uuid4()->toString();
        $user->setToken($token);
        $this->userRepository->save($user);
        return $token;
    }
}
