<?php
namespace App\Security;

use App\Domain\Model\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiAuthenticator extends AbstractGuardAuthenticator
{
    private $em;

    protected $protectionMap = [
        '^(/api/article)(.*)' => [Request::METHOD_DELETE, Request::METHOD_POST, Request::METHOD_PUT],
        '^(/api/category)(.*)' => [Request::METHOD_DELETE, Request::METHOD_POST, Request::METHOD_PUT],
    ];

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        foreach ($this->protectionMap as $pattern => $httpVerbs) {
            $result = \preg_match("@" . $pattern . "@", $request->getPathInfo(), $match);
            if ($result && in_array($request->getMethod(), $httpVerbs)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     * @param Request $request
     * @return array
     */
    public function getCredentials(Request $request)
    {
        if (!$request->headers->has('API-TOKEN')) {
            throw new CustomUserMessageAuthenticationException(
                'Authentication Required'
            );
        }
        return [
            'token' => $request->headers->get('API-TOKEN')
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $apiToken = $credentials['token'];

        if (empty($apiToken)) {
            throw new CustomUserMessageAuthenticationException(
                'Cannot read authenticate token'
            );
        }

        $user = $this->em->getRepository(User::class)
            ->findOneBy(['token' => $apiToken]);

        if (!$user) {
            throw new CustomUserMessageAuthenticationException(
                'Invalid api token'
            );
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];
        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}


