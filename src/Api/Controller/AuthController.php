<?php

namespace App\Api\Controller;

use App\Dto\Token;
use App\Security\TokenService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends AbstractFOSRestController
{
    /**
     * @var TokenService
     */
    protected $tokenService;

    /**
     * AuthController constructor.
     * @param TokenService $tokenService
     */
    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    /**
     * Generate api token
     *
     * @Rest\Post("/auth/token")
     * @Rest\View(serializerGroups={"api"})
     * @ParamConverter("tokenDto", converter="fos_rest.request_body")
     * @param Token $tokenDto
     * @return View
     */
    public function token(Token $tokenDto)
    {
        $token = $this->tokenService->create($tokenDto);
        return View::create(['token' => $token], Response::HTTP_OK);
    }
}
