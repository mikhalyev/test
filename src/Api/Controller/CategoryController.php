<?php

namespace App\Api\Controller;

use App\Contract\CategoryServiceInterface;
use App\Contract\PolicyStrategyFilterInterface;
use App\Dto\Category AS CategoryDto;
use App\Common\Criteria;
use App\Common\Paginator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends AbstractFOSRestController
{
    /**
     * @var CategoryServiceInterface
     */
    protected $categoryService;

    /**
     * CategoryController constructor.
     * @param CategoryServiceInterface $categoryService
     */
    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Get a list of articles.
     * Your can return list of articles defined by page, size of page and context search string.
     * @Rest\Get("/category")
     *
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Number of returned page.")
     * @Rest\QueryParam(name="size", requirements="\d+", default="10", description="The number of categories returned per page.")
     * @Rest\View(serializerGroups={"api"})
     */
    public function listCategories(ParamFetcher $paramFetcher) :View
    {
        $criteria = new Criteria();
        $criteria->setPage($paramFetcher->get('page'));
        $criteria->setSize($paramFetcher->get('size'));

        $paginator = new Paginator();
        $this->categoryService->search($criteria, $paginator);
        return View::create($paginator, Response::HTTP_OK);
    }

    /**
     * Get category by ID.
     * @Rest\Get("/category/{id}")
     * @Rest\View(serializerGroups={"api"})
     */
    public function getCategory(int $id): View
    {
        $category = $this->categoryService->getById($id);
        if (!$category) {
            return View::create($category, Response::HTTP_NOT_FOUND);
        }
        return View::create($category, Response::HTTP_OK);
    }

    /**
     * Create new category
     * @Rest\Post("/category")
     * @ParamConverter("categoryDto", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"api"})
     * @return View
     */
    public function newCategory(CategoryDto $categoryDto) : View
    {
        $category = $this->categoryService->create($categoryDto);
        return View::create($category, Response::HTTP_CREATED);
    }

    /**
     * Update category by ID.
     * @Rest\Put("/category/{id}")
     * @ParamConverter("categoryDto", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"api"})
     * @return View
     */
    public function updateCategory(CategoryDto $categoryDto, int $id): View
    {
        $category = $this->categoryService->update($id, $categoryDto);
        return View::create($category, Response::HTTP_OK);
    }

    /**
     * Update an category
     * @Rest\Delete("/category/{id}")
     * @return View
     */
    public function removeCategory(int $id): View
    {
        $this->categoryService->remove($id);
        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
