<?php

namespace App\Api\Controller;

use App\Exception\InvalidDataException;
use App\Exception\ModelNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController extends AbstractFOSRestController
{
    /**
     * Converts an Exception to a Response.
     *
     * @param Request $request
     * @param  $exception
     *
     * @return Response
     */
    public function handle(Request $request, $exception)
    {
        $isDevMode = $request->server->get('APP_ENV', 'prod') == 'dev';
        $isDebugMode = $request->server->getBoolean('APP_DEBUG', false);

        $defaultCode = Response::HTTP_INTERNAL_SERVER_ERROR;

        switch ($exception->getClass()) {
            case ModelNotFoundException::class: {
                $code = Response::HTTP_NOT_FOUND;
                $message = $exception->getMessage();
                break;
            }
            case InvalidDataException::class: {
                $code = Response::HTTP_BAD_REQUEST;
                $message = $exception->getMessage();
                break;
            }
            default: {
                $code = (array_key_exists($exception->getCode(), Response::$statusTexts)) ? $exception->getCode() : $defaultCode;
                $message = $isDevMode ? $exception->getMessage() : Response::$statusTexts[$defaultCode];
            }
        }

        $responseData = [
            'message' => $message
        ];

        if ($isDebugMode) {
            $responseData['debug'] = [
                'message' => $exception->getMessage(),
                'stackTrace' => $exception->getTrace(),
            ];
        }
        return $this->json($responseData, $code);
    }
}

