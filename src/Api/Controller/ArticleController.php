<?php

namespace App\Api\Controller;

use App\Dto\Article AS ArticleDto;
use App\Common\Criteria;
use App\Common\Paginator;
use App\Contract\ArticleServiceInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractFOSRestController
{
    /**
     * @var ArticleServiceInterface
     */
    protected $articleService;

    /**
     * ArticleController constructor.
     *
     * @param ArticleServiceInterface $articleService
     */
    public function __construct(ArticleServiceInterface $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
     * Get a list of articles.
     * Your can return list of articles defined by page, size of page and context search string.
     *
     * @Rest\Get("/article")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Number of returned page.")
     * @Rest\QueryParam(name="size", requirements="\d+", default="10", description="The number of articles returned per page.")
     * @Rest\QueryParam(name="q", default="", description="Context search string.")
     * @Rest\QueryParam(name="cat", requirements="\d+", default="", description="Id category.")
     * @Rest\View(serializerGroups={"api"})
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function listArticles(ParamFetcher $paramFetcher)
    {
        $criteria = new Criteria();
        $criteria->setPage($paramFetcher->get('page'));
        $criteria->setSize($paramFetcher->get('size'));
        $criteria->addFilter(ArticleServiceInterface::FILTER_CODE_CONTEXT_SEARCH, $paramFetcher->get('q'));
        $criteria->addFilter(ArticleServiceInterface::FILTER_CODE_CATEGORY, $paramFetcher->get('cat'));

        $paginator = new Paginator();
        $this->articleService->search($criteria, $paginator);
        return View::create($paginator, Response::HTTP_OK);
    }

    /**
     * Get article with ID.
     *
     * @Rest\Get("/article/{id}")
     * @Rest\View(serializerGroups={"api"})
     * @param int $id
     * @return View
     * @throws \Exception
     */
    public function getArticle(int $id): View
    {
        $article = $this->articleService->getById((int)$id);
        if (!$article) {
            return View::create($article, Response::HTTP_NOT_FOUND);
        }
        return View::create($article, Response::HTTP_OK);
    }

    /**
     * Create new article
     *
     * @Rest\Post("/article")
     * @ParamConverter("articleDto", converter="fos_rest.request_body")
     * @param ArticleDto $articleDto
     * @return View
     * @Rest\View(serializerGroups={"api"})
     */
    public function newArticle(ArticleDto $articleDto)
    {
        $article = $this->articleService->create($articleDto);
        return View::create($article, Response::HTTP_CREATED);
    }

    /**
     * Update article with ID.
     * @Rest\Put("/article/{id}")
     * @ParamConverter("articleDto", converter="fos_rest.request_body")
     * @param ArticleDto $articleDto
     * @param int $id
     * @return View
     * @Rest\View(serializerGroups={"api"})
     */
    public function updateArticle(ArticleDto $articleDto, int $id): View
    {
        $article = $this->articleService->update($id, $articleDto);
        return View::create($article, Response::HTTP_OK);
    }

    /**
     * Remove an article
     * @Rest\Delete("/article/{id}")
     * @param int $id
     * @return View
     */
    public function removeArticle(int $id): View
    {
        $this->articleService->remove($id);
        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
