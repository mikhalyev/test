Need copy .env.template to .env and set params git Dd

#Init docker containers 
docker-compose build
docker-compose up -d

#Install
got  into container
   
    docker exec -it article-service-phpfpm  bash

Install restrictions in container 

    composer install
    
Run migrations

    bin/console doctrine:migrations:migrate
    
RUN Project     

The project will be available at URL: localhost

Api documentation on url http://localhost/api/doc.json

Postman Api collection in file "ArticlesApi.postman_collection.json"
 
    
